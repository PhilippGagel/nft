const width = 1280;
let height = 720;

let context;
let streaming = false;
let intervalID;

let ready = false;
let captureMat, gray, blurred, thresholded;
let contours, hierarchy;
let alphaMat, resetMat;
let alpha = 0.8;
let alphaInvert = 1 - alpha;

let FPSTimer = new Date().getTime();
let FPSCounter = 0;
let lastFPS;

let showContours = document.getElementsByClassName("showContours")[0];
let thresholdElem = document.getElementsByClassName("threshold")[0];
let blurRadiusElem = document.getElementsByClassName("blurRadius")[0];
let showThreshold = document.getElementsByClassName("showThreshold")[0];
let showFPS = document.getElementsByClassName("showFPS")[0];

let mouseOverCanvasCoords;
let mouseOverCanvas = false;
let canvas = document.getElementsByClassName("c")[0];
let rect = canvas.getBoundingClientRect();
let selectedContour = -1;
let hoveredContour = -1;

function cvSetup() {
  captureMat = new cv.Mat([height, width], cv.CV_8UC4);
  alphaMat = new cv.Mat.zeros([height, width], cv.CV_8UC3);
  resetMat = new cv.Mat.zeros([height, width], cv.CV_8UC3);
  gray = new cv.Mat([height, width], cv.CV_8UC1);
  blurred = new cv.Mat([height, width], cv.CV_8UC1);
  thresholded = new cv.Mat([height, width], cv.CV_8UC1);
}

function cvReady() {
  if (ready) return true;
  if (typeof(cv) == "undefined" && !cv.loaded) return false;
  cvSetup();
  ready = true;
  return true;
}

function setup() {
  let constraints = {
    audio: false,
    video: {
      width: {
        ideal: width
      },
      height: {
        ideal: height
      }
    }
  };

  let play = document.getElementsByClassName('play')[0];
  let pause = document.getElementsByClassName('pause')[0];

  canvas.onmousemove = function(event) {
    mousemoveCanvas(event);
  };
  canvas.onclick = function() {
    mouseclickCanvas()
  };
  canvas.onmouseenter = function() {
    mouseEnteredCanvas();
  };
  canvas.onmouseleave = function() {
    mouseLeftCanvas();
  };

  // Video Setup
  navigator.mediaDevices.getUserMedia(constraints)
    .then(function(mediaStream) {

      let video = document.querySelector('video');
      let canvas = document.querySelector('canvas');

      if (video) {
        video.srcObject = mediaStream;

        context = canvas.getContext('2d');

        play.addEventListener('click', function(e) {
          video.play();
        });

        pause.addEventListener('click', function(e) {
          clearInterval(intervalID);
          video.pause();
        });

        video.addEventListener('canplay', function(ev) {
          if (!streaming) {
            height = video.videoHeight / (video.videoWidth / width);

            canvas.setAttribute('width', width);
            canvas.setAttribute('height', height);

            context.translate(width, 0);
            context.scale(-1, 1);

            streaming = true;
          }
        }, false);

        video.addEventListener('play', function() {
          if (intervalID) {
            clearInterval(intervalID);
          }

          intervalID = setInterval(function() {
            if (video.paused || video.ended) return;

            context.fillRect(0, 0, width, height);
            context.drawImage(video, 0, 0, width, height);

            if (cvReady()) {
              compute();
            }
          }, 33);
        }, false);
      } else {
        console.log("Error: No video tag found. ");
      }

      video.onloadedmetadata = function(e) {
        video.play();
      };
    })
    .catch(function(err) {
      console.log(err.name + ": " + err.message);
    });
}

function compute() {
  let imageData = context.getImageData(0, 0, width, height);
  let data = imageData.data;

  if (data.length > 0) {

    captureMat.data().set(data);

    if (showContours.checked) {
      computeContours();
    }
    if (showFPS.checked) {
      computeFPS();
    }

    data.set(captureMat.data())
  }

  context.putImageData(imageData, 0, 0);
}

function computeFPS() {
  FPSCounter++;
  let now = new Date().getTime();

  if (now > FPSTimer + 1000) {
    lastFPS = FPSCounter;
    FPSCounter = 0;
    FPSTimer = now;
  }

  cv.putText(captureMat, "FPS: "+lastFPS.toString(), [30, 50], 0, 1.3, new cv.Scalar(255, 0, 0, 255), 3, 8, false);
}

function computeContours() {
  let threshold = parseInt(thresholdElem.value);
  let blurRadius = parseInt(blurRadiusElem.value);

  cv.cvtColor(captureMat, gray, cv.ColorConversionCodes.COLOR_RGBA2GRAY.value, 0);
  cv.blur(gray, blurred, [blurRadius, blurRadius], [-1, -1], cv.BORDER_DEFAULT);
  //cv.medianBlur(gray, blurred, blurRadius);
  cv.threshold(blurred, thresholded, threshold, 255, cv.ThresholdTypes.THRESH_BINARY.value);

  if (showThreshold.checked) {

    let src = thresholded.data();
    let dest = captureMat.data();
    let size = src.length;
    let j = 0;

    for (let i = 0; i < size; i++) {
      dest[j++] = src[i];
      dest[j++] = src[i];
      dest[j++] = src[i];
      j++;
    }
  } else {

    contours = new cv.MatVector();
    hierarchy = new cv.Mat();

    alphaMat.data().set(resetMat.data());

    cv.findContours(
      thresholded /*Matrix to use for computation*/ ,
      contours /*matrix array to save contours*/ ,
      hierarchy /*matrix to store the hierarchy of the different contours (one inside another)*/ ,
      3 /*Mode to use for detection*/,
      2 /*Method to use for detection*/,
      [0, 0] /*offset of contour points (optional)*/
    );

    let color = new cv.Scalar(0, 0, 255);
    let fillColor = new cv.Scalar(255, 0, 255);

    if (contours) {
      cv.drawContours(
        alphaMat /*matrix to draw in*/ ,
        contours /*contour array*/ ,
        -1 /*draw all countours (positive if only one specific)*/,
        color /*Color*/ ,
        2 /*Line Thickness*/ ,
        8 /*draw them as Anti Aliased Line*/ ,
        hierarchy /*Hierarchy matrix*/ ,
        2147483647 /*how many levels of contours should be drawn (INT_MAX in this case)*/,
        [0, 0] /*offest*/ );


      if (mouseOverCanvas) {
        let contourToFill = getContourFromMousePosition(contours);
        if (contourToFill != "undefined") {

          cv.floodFill(
            alphaMat /*matrix to fill*/,
            mouseOverCanvasCoords /*seed point at which fill starts*/,
            fillColor /*color to fill with*/,
            new cv.Rect([0, 0], [0, 0])/*output parameter to store bounding rectangle*/,
            new cv.Scalar(0) /*maximum allowed lower brightness to still fill the pixel*/,
            new cv.Scalar(0) /*maximum allowed higher brightness to still fill the pixel*/,
            cv.FloodFillFlags.FLOODFILL_FIXED_RANGE.value) /*Consider color difference between seed and current pixel to see if it is still filled*/;

          contourToFill.delete();
        }
        cv.circle(alphaMat, mouseOverCanvasCoords, 3, new cv.Scalar(0, 255, 0), 4, 8, 0);
      }

      color.delete();
    }

    let src = alphaMat.data();
    let dest = captureMat.data();
    let i = src.length, j = dest.length;
    let alpha = 3;

    while(j--){
        if(alpha == 3){
          alpha = 0;
          dest[j] = 255;
        }
        else{
          i--;
          if(src[i] != 0) dest[j] = src[i] * alpha + dest[j] * alphaInvert;
          alpha++;
        }
    }

    contours.delete();
    hierarchy.delete();
  }
}

function getContourFromMousePosition(contours) {
  for (let i = 0; i < contours.size(); i++) {
    let contour = contours.get(i);

    if (cv.pointPolygonTest(contour, mouseOverCanvasCoords, false) > 0) {
      let candidate = new cv.MatVector();
      hoveredContour = i;
      candidate.push_back(contour);
      return candidate;
    }
  }

  hoveredContour = -1;

  return "undefined";
}

function mouseclickCanvas() {
  selectedContour = hoveredContour;
}

function mouseEnteredCanvas() {
  mouseOverCanvas = true;
}

function mouseLeftCanvas() {
  mouseOverCanvas = false;
}

function mousemoveCanvas(event) {
  let relativeX = event.clientX - rect.left;
  let relativeY = event.clientY - rect.top;
  mouseOverCanvasCoords = [relativeX, relativeY];
}

function cleanup() {
  captureMat.delete();
  thresholded.delete();
  blurred.delete();
  gray.delete();
}
