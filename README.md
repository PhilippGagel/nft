# Master Project - NFT #

### What is this repository for? ###

First of all, this is part of a students project which takes place in the master studies of Philipp Gagel.

This project uses the users webcam if availbale to scan the view for natural features.
These features should later be used to display 3 dimensional objects on specific places in the room.
To accomplish this the OpenCV binding **OpenCVJS** should be used.

### How do I get set up? ###

At first node.js should be installed on the system.

After that just execute the following steps:

`cd PATH\TO\NFT`

`npm install`

`npm start`

Now open your browser and enter

localhost:3000/camera

into your addres bar. This should lead to a webpage which will ask you for your permission to access your camera if there is any. Just accept the request and your webcams view should be visible on screen.

### Usage ###

At this point the function consists of configurable contour computation and highlightning of hovered contours. The hover function was added to see how to identify a contour on the 2D plane via mouse position.
To see the raw images and maximal FPS you can turn off the contour computation. The computation is turned off by default. To see the contours you might turn the switch as soon as opencvjs files are loaded into your cache.
You can adjust the threshold and blur radius of the contour finder in a specified range to fit your needs. Apart from that you may display the current frames per second to see how well the site performs on your PC.
